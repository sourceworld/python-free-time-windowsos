# Python Free Time WindowsOS

```python

#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
os.system('mode con:cols=63 lines=30')
import ctypes
from colorconsole import terminal
os.system('title SourceWorld (Timer)')
screen = terminal.get_terminal(conEmu=False)
def banner():
    screen.cprint(4, 0,"")
    print "   _____                       __          __        _     _   "
    print "  / ____|                      \ \        / /       | |   | |  "
    print " | (___   ___  _   _ _ __ ___ __\ \  /\  / /__  _ __| | __| |  "
    print "  \___ \ / _ \| | | | '__/ __/ _ \ \/  \/ / _ \| '__| |/ _` |  "
    print "  ____) | (_) | |_| | | | (_|  __/\  /\  / (_) | |  | | (_| |  "
    print " |_____/ \___/ \__,_|_|  \___\___| \/  \/ \___/|_|  |_|\__,_|  "
    print "                                                               "
    screen.cprint(8, 0,"")
    print "==============================================================="
    screen.cprint(4, 0,"")
    print "        -=-  SourceWorld @ all copyrights reserved  -=-        "
    screen.cprint(8, 0,"")
    print "==============================================================="
    screen.cprint(9, 0,"")
    print " M = Minute"
    print " H = Hour"
    print "=============="
    print " USE a number and M or H like 5M (5M = 5 Minute)"
    print "=============="
    print " S = Stop"
    print "=============="
banner()
def timer_def():
    screen.cprint(6, 0,"")
    down = raw_input('Shutdown : ')

    comp_string = down
    new_string = ''.join([i for i in comp_string if not i.isdigit()])


    if new_string == "m" or new_string == "M":
        down = down.replace(new_string,'')
        time_s = int(down) * 60
        os.system('shutdown -a')
        os.system('shutdown -s -f -t '+str(time_s))


    if new_string == "h" or new_string == "H":
        down = down.replace(new_string,'')
        time_s = int(down) * 3600
        os.system('shutdown -a')
        os.system('shutdown -s -f -t '+str(time_s))
        

    if new_string == "s" or new_string == "S":
        down = down.replace(new_string,'')
        os.system('shutdown -a')

    ctypes.windll.user32.ShowWindow( ctypes.windll.kernel32.GetConsoleWindow(), 6 )

    timer_def()

timer_def()

```